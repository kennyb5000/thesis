var init = function(){
    $(".chat_small").draggable({axis:"x", containment:"body"})
    $(".chat_small").click(function(){
        $(".chat_dialog").dialog({
            title:"Room Chat",
            buttons:{"Comment":function(){
                    socket.emit("msg", {msg:$(".comment").val(), user:$.cookie("name")})
                     $(".chat_posts").append("<div class='mypost'><div class='user inline'>Me:</div><div class='msg inline triangle-right left'>"+$(".comment").val().replace(/\n/g, "<br/>")+"</div></div>");
                     $(".comment").val("")
                },
                "Close":function(){
                    $(".num_new").html("0");
                    $(".chat_dialog").dialog("close")
                }
            }
        });
    });
    
    $("#toggle_all_sprint").click(function(e){
    e.stopPropagation();
        var event_class = $(this).attr("class");
        var that = this;
        var expand = false;
        if(event_class.indexOf("ui-icon-circle-minus")>-1){
            expand = false;
            $(that).removeClass("ui-icon-circle-minus");
            $(that).addClass("ui-icon-circle-plus");
        }else{
            expand = true;
            $(that).removeClass("ui-icon-circle-plus");
            $(that).addClass("ui-icon-circle-minus");
        }
        $("#tabs-3 .Story .ui-accordion-header").each(function(){
            var item_class = $(this).attr("class");
            if(expand && item_class.indexOf("ui-accordion-header-active") === -1){
                $(this).click();
            }else if(expand === false && item_class.indexOf("ui-accordion-header-active") > -1){
                $(this).click();
            }
        });
    });
    $("#toggle_all_user_stories").click(function(e){
    e.stopPropagation();
        var event_class = $(this).attr("class");
        var that = this;
        var expand = false;
        if(event_class.indexOf("ui-icon-circle-minus")>-1){
            expand = false;
            $(that).removeClass("ui-icon-circle-minus");
            $(that).addClass("ui-icon-circle-plus");
        }else{
            expand = true;
            $(that).removeClass("ui-icon-circle-plus");
            $(that).addClass("ui-icon-circle-minus");
        }
        $("#tabs-5 .Story .ui-accordion-header").each(function(){
    e.stopPropagation();
            var item_class = $(this).attr("class");
            if(expand && item_class.indexOf("ui-accordion-header-active") === -1){
                $(this).click();
            }else if(expand === false && item_class.indexOf("ui-accordion-header-active") > -1){
                $(this).click();
            }
        });
    });
    $("#toggle_all_project").click(function(e){
        var event_class = $(this).attr("class");
        var that = this;
        var expand = false;
        if(event_class.indexOf("ui-icon-circle-minus")>-1){
            expand = false;
            $(that).removeClass("ui-icon-circle-minus");
            $(that).addClass("ui-icon-circle-plus");
        }else{
            expand = true;
            $(that).removeClass("ui-icon-circle-plus");
            $(that).addClass("ui-icon-circle-minus");
        }
        $("#tabs-4 .Story .ui-accordion-header").each(function(){
            var item_class = $(this).attr("class");
            if(expand && item_class.indexOf("ui-accordion-header-active") === -1){
                $(this).click();
            }else if(expand === false && item_class.indexOf("ui-accordion-header-active") > -1){
                $(this).click();
            }
        });
    });
    $("#add-new-user").button()
    .click(function(){
        $("#user-dialog").dialog({
                    modal : true,
                    draggable : false,
                    resizable : false,
                    width : 800,
                    position : {
                            my : "left top",
                            at : "left top",
                            of : ".user-story-header",
                            within : "#tabs-6"
                    }
            })
            
    })
    $("#save_user").button()
    .click(function(){
        var form = $("#user-dialog form");
        var finitial = $("[name='FirstName']", form).val();
        var lastName = $("[name='LastName']", form).val();
        var company = $("[name='Company']", form).val();
        var login = finitial + lastName;
        var data = {};
        $("input, select, textarea", form).each(function(){
            var name = $(this).attr("name");
            var val = $(this).val();
            data[name] = val;
            $(this).val("");
        });
        data["Username"] = login;
        socket.emit("createNewUser", data);
        $( this ).closest("#user-dialog").dialog( "close" );
    })
    $("#cancel_user").button()
    .click(function(){
        var form = $("#user-dialog form");
        $( this ).closest("#user-dialog").dialog( "close" );
         $("input, select, textarea", form).each(function(){
            $(this).val("");
        });
    })
    $("#tabs").tabs({
        show: function(event, ui) {
            $('#calendar').fullCalendar('render');
                        if(ui.panel.id === "tabs-6" && !$.fn.DataTable.fnIsDataTable( jQuery('#manage_table_id')[0] )){
                         $('#manage_table_id').dataTable({
                        "bJQueryUI" : true,
                        "aoColumns": [
                      { "sWidth": "20%" },
                      { "sWidth": "20%" },
                      { "sWidth": "20%" },
                      { "sWidth": "20%" },
                      { "sWidth": "20%" }
                    ]
                });	
                        }else if(ui.panel.id === "tabs-1" && !$.fn.DataTable.fnIsDataTable( jQuery('#task_table')[0] )){
                        $('#task_table').dataTable({
                        "bJQueryUI" : true
                });
                        }else if(ui.panel.id === "tabs-2" && !$.fn.DataTable.fnIsDataTable( jQuery('#task_table_sprint')[0] )){
                        $('#task_table_sprint').dataTable({
                        "bJQueryUI" : true
                });
                        }
        }
    });
    $("#add-new-ustory")
    .button()
    .click(function(){
        $("#user_story_dialog").dialog({
                    modal : true,
                    draggable : false,
                    resizable : false,
                    width : 800,
                    position : {
                            my : "left top",
                            at : "left top",
                            of : ".user-story-header",
                            within : "#tabs-5"
                    }
            })
         var form = jQuery("#user_story_dialog").children("form");
         $("[name='isNew']", form).val("true");
         validate_form(form, false);
    });

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    calendar = $('#calendar');
    $('#calendar').fullCalendar({
            theme : true,height : 250,slotMinutes : 30,firstHour : 8,minTime : 4,
            header : {
                    left : 'prev,next today',
                    center : 'title',
                    right : 'basicWeek,agendaWeek,agendaDay'
            },
            defaultView:"agendaWeek",
            buttonText : {
                    agendaWeek : 'week agenda'
            },selectable : true,selectHelper : true,
            select : function (start, end, allDay, jsEvent, view) {
                     $( "#dialog-calendar .start-date-new" ).html($.fullCalendar.formatDate(start, "ddd MMMM ddS hh:mm tt"));
                     $( "#dialog-calendar .end-date-new" ).html($.fullCalendar.formatDate(end, "ddd MMMM ddS hh:mm tt"));
                     $( "#dialog-calendar" ).dialog({
                        resizable: false,
                        height:200,
                        width:450,
                        modal: true,
                        buttons: {
                            Save: function(){
                                var title = $("#dialog-calendar .title").val();
                                var guests = $("#dialog-calendar .guests").val();
                                if(title !== ""){
                                    socket.emit("add_calendar_item", {title: title,start: $.fullCalendar.formatDate(start, "u"),end: $.fullCalendar.formatDate(end, "u"),guests: guests, allDay:allDay}, function(id, data){
                                        data["id"] = "cal_"+parseInt(id);
                                        data["start"] = $.fullCalendar.parseDate( data["start"] )
                                        data["end"] = $.fullCalendar.parseDate( data["end"] )
                                        calendar.fullCalendar('renderEvent',data,true)
                                    })
                                }else{
                                  $( "#dialog-calendar .start-date-new" ).html("");
                                  $( "#dialog-calendar .end-date-new" ).html("");
                                  $( "#dialog-calendar input" ).each(function(){
                                    $(this).html("");
                                  });
                                }
                                $( this ).dialog( "close" );
                           },
                           Cancel: function() {
                                $( "#dialog-calendar .start-date-new" ).html("");
                                $( "#dialog-calendar .end-date-new" ).html("");
                                $( "#dialog-calendar input" ).each(function(){
                                    $(this).html("");
                                });
                                $( this ).dialog( "close" );
                            }
                        }
                    });
                    calendar.fullCalendar('unselect');
            },editable : true,
            eventAfterRender : function (event, element, view) {
                    var today = new Date();
                    var diff = parseInt((event.start - today) / (24 * 3600 * 1000));
                    if (diff === 0) {
                            $(element).removeClass("fc-event-skin");
                            $(element).children(".fc-event-skin").removeClass("fc-event-skin");
                            $(element).css("background-color", "#C66633 !important");
                            $(element).css("border-color", "#C66633 !important");
                    } else if (diff > 0 && diff < 3) {
                            $(element).removeClass("fc-event-skin");
                            $(element).children(".fc-event-skin").removeClass("fc-event-skin");
                            $(element).css("background-color", "#6633CC !important");
                            $(element).css("border-color", "#6633CC !important");
                    } else if (diff < 0) {
                            $(element).removeClass("fc-event-skin");
                            $(element).children(".fc-event-skin").removeClass("fc-event-skin");
                            $(element).css("background-color", "#666666 !important");
                            $(element).css("border-color", "#666666 !important");
                    }
                    $(".fc-event-head", element).append(trash_html+'<div name="id" class="hidden">'+event.id+'</div>');
                    $(".fc-event-head img", element).live("click", function(){
                        var id = $(this).parent().children("[name='id']").text().split("_")[1];
                        var name = $(".fc-event-title", $(this).closest(".fc-event-inner")).text();
                        var msg = "This item will be removed form you calendar and all others. Are you sure?";
                        $(".delete_msg").html(msg);
                        $( "#dialog-confirm" ).dialog({
                            resizable: false,
                            height:180,
                            modal: true,
                            buttons: {
                                "Delete item": function() {
                                    socket.emit("delete_item", {table:'Calendar', id:id, name:name})
                                    calendar.fullCalendar( 'removeEvents' , id )
                                    $( this ).dialog( "close" );
                                },
                                Cancel: function() {
                                    $( this ).dialog( "close" );
                                }
                            }
                        }); 
                    })
            },
            eventRender: function(event, element) {
                if(event.id.indexOf("cal")>-1){
                    element.bind('dblclick', function() {
                       var title = event.title, start = event.start, end = event.end, guests = event.guests, allDay = event.allDay, id = event.id
                       $( "#dialog-calendar .start-date-new" ).html($.fullCalendar.formatDate(event.start, "ddd MMMM ddS hh:mm tt"));
                       $( "#dialog-calendar .end-date-new" ).html($.fullCalendar.formatDate(event.end, "ddd MMMM ddS hh:mm tt"));
                       $( "#dialog-calendar .title" ).val(event.title);
                       $( "#dialog-calendar .guests" ).val(event.guests);
                       $( "#dialog-calendar" ).dialog({
                            resizable: false,
                            height:200,
                            width:450,
                            modal: true,
                            buttons: {
                                Save: function(){
                                    var title = $("#dialog-calendar .title").val();
                                    var guests = $("#dialog-calendar .guests").val();
                                    if(title !== ""){
                                        socket.emit("update_calendar_item", {id: id.split("_")[1],title: title,start: $.fullCalendar.formatDate(event.start, "u"),end: $.fullCalendar.formatDate(event.end, "u"),guests: guests, allDay:allDay}, function(id, data){
                                            data["id"] = "cal_"+parseInt(id);
                                            data["start"] = $.fullCalendar.parseDate( data["start"] )
                                            data["end"] = $.fullCalendar.parseDate( data["end"] )
                                            calendar.fullCalendar( 'removeEvents' , "cal_"+parseInt(id) )
                                            calendar.fullCalendar('renderEvent',data,true)
                                        })
                                    }else{
                                      $( "#dialog-calendar .start-date-new" ).html("");
                                      $( "#dialog-calendar .end-date-new" ).html("");
                                      $( "#dialog-calendar input" ).each(function(){
                                        $(this).val("");
                                      });
                                    }
                                    $( this ).dialog( "close" );
                               },
                               Cancel: function() {
                                    $( "#dialog-calendar .start-date-new" ).html("");
                                    $( "#dialog-calendar .end-date-new" ).html("");
                                    $( "#dialog-calendar input" ).each(function(){
                                        $(this).val("");
                                    });
                                    $( this ).dialog( "close" );
                                }
                            }
                        });
                    });
                }
             },
             eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
                var title = event.title, start = event.start, end = event.end, guests = event.guests, allDay = event.allDay, id = event.id
                if(title !== ""){
                    socket.emit("update_calendar_item", {id: id.split("_")[1],title: title,start: $.fullCalendar.formatDate(event.start, "u"),end: $.fullCalendar.formatDate(event.end, "u"),guests: guests, allDay:allDay}, function(id, data){
                        data["id"] = "cal_"+parseInt(id);
                        data["start"] = $.fullCalendar.parseDate( event.start )
                        data["end"] = $.fullCalendar.parseDate( event.end )
                    })
                }
             },
             eventResize: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
                var title = event.title, start = event.start, end = event.end, guests = event.guests, allDay = event.allDay, id = event.id
                if(title !== ""){
                    socket.emit("update_calendar_item", {id: id.split("_")[1],title: title,start: $.fullCalendar.formatDate(event.start, "u"),end: $.fullCalendar.formatDate(event.end, "u"),guests: guests, allDay:allDay}, function(id, data){
                        data["id"] = "cal_"+parseInt(id);
                        data["start"] = $.fullCalendar.parseDate( event.start )
                        data["end"] = $.fullCalendar.parseDate( event.end )
                    })
                }
             }
    });
    
  clickies();
}
var accord = function(){    
    $( ".accordion > div.acChild" ).accordion({
        collapsible: true,
        active:false,
        heightStyle:"content"
    });
    $( ".accordion-story > div.acChild" ).accordion({
        collapsible: true,
        active:0,
        heightStyle:"content",
        beforeActivate: function( event, ui ) {
            if(ui.newHeader.length === 0 && ui.newPanel.length === 0){
                var parenter = $(this).closest("tr");
                $("td:not(:nth-child(1))", parenter).each(function(){
                  var Number = $(this).children().children().length;
                  $(this).children().toggle("slow")
                  $(this).prepend("<div class='number'>"+Number+" Tasks</div>")
                })
            }else{
              var parenter = $(this).closest("tr");
                $("td:not(:nth-child(1))", parenter).each(function(){
                  $(this).children().toggle("slow")
                  $(this).children(".number").remove();
                })
            }
        }
    });
    clickies();
}
var clickies = function(){
   $("#tabs-5 .story-data .ui-icon-pencil").unbind("click");
   $("#tabs-5 .task-data .ui-icon-pencil").unbind("click");
   $("#tabs-5 .task-content .ui-icon-trash").unbind("click");
   $("#tabs-5 .story-data .ui-icon-note").unbind("click");
   $(".existing-sprint .ui-icon-trash").unbind("click");
   $("#tabs-5 .story-data .ui-icon-trash").unbind("click");
   $("#tabs-5 .story-data .ui-icon-pencil").click(function(e){
      e.stopPropagation();
      toggleLoad(true);
      var story_id = $(this).closest("h3").next().children("div").attr("data-storyid");
      socket.emit("get_info", {table:'UserStories', id:story_id}, function(data){
        setup_dialog("#user_story_dialog", data, "#tabs-5")
      })
  });
  $("#tabs-5 .task-data .ui-icon-pencil").click(function(e){
    e.stopPropagation();
    toggleLoad(true);
    var data_properties = $.parseJSON($(this).closest("div").find("[data-prop]").attr("data-prop"));
    var story_id = data_properties.story_id, task_id = data_properties.task_id;
    socket.emit("get_info", {table:'Tasks', id:task_id}, function(data){
         setup_dialog("#task_dialog", data, "#tabs-5")
     })
 });
  $("#tabs-5 .task-data .ui-icon-trash").click(function(e){
    e.stopPropagation();
    toggleLoad(true);
    var data_properties = $.parseJSON($("[data-prop]",$(this).closest("h3").closest("div")).attr("data-prop"));
    var story_id = data_properties.story_id, task_id = data_properties.task_id;
    var index = $(this).closest("td").index();
    var name = $(".story-title", $(this).closest("div")).text();
    if(index === 2){
        var msg = "You cannot delete a task that is complete.";
        $(".delete_msg").html(msg);            
        $( "#dialog-confirm" ).dialog({
            resizable: false,
            height:180,
            modal: true,
            buttons: {
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });   
    }else{
        var msg = "This item will be permanently deleted and cannot be recovered. Are you sure?";
        $(".delete_msg").html(msg);
        $( "#dialog-confirm" ).dialog({
            resizable: false,
            height:180,
            modal: true,
            buttons: {
                "Delete item": function() {
                    socket.emit("delete_item", {table:'Tasks', id:task_id, name:name})
                    $( this ).dialog( "close" );
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });   
    }   
 });
 $(".existing-sprint .ui-icon-trash").click(function(e){
     var sprint_id = $(this).parent()[0].attributes["data-sprintid"].nodeValue;
     var name = $(this).parent().children(".sprint-name-list").text().split(":")[0];
     socket.emit("check_sprint", {id:sprint_id, name:name}, function(data, id, name){
         if(data>0){
            var msg = "You cannot delete a Sprint that includes tasks.";
            $(".delete_msg").html(msg);            
            $( "#dialog-confirm" ).dialog({
                resizable: false,
                height:180,
                modal: true,
                buttons: {
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }
            });   
        }else{
            var msg = "This Sprint will be permanently deleted and cannot be recovered. Are you sure?";
            $(".delete_msg").html(msg);
            $( "#dialog-confirm" ).dialog({
                resizable: false,
                height:180,
                modal: true,
                buttons: {
                    "Delete item": function() {
                        socket.emit("delete_item", {table:'Sprints', id:id, name:name})
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                }
            });   
        }
     })
 })
  $("#tabs-5 .story-data .ui-icon-note").click(function(e){
    e.stopPropagation();
    var story_id = $(this).closest("h3").next().children("[data-storyid]").attr("data-storyid");
    $("#task_dialog").dialog({
            modal : true,
            draggable : false,
            resizable : false,
            width : 800,
            position : {
                    my : "left top",
                    at : "left top",
                    of : ".user-story-header",
                    within : "#tabs-5"
            }
     })
     var form = jQuery("#task_dialog").children("form");
     $("[name='isNew']", form).val("true");
     $("[name='id_UserStory']", form).val(story_id);
     validate_form(form, false);

  });
  $("#tabs-5 .story-data .ui-icon-trash").click(function(e){
    e.stopPropagation();
    var story_id = $(this).closest("h3").next().children("[data-storyid]").attr("data-storyid");
    var completed_tasks = $($(this).closest("tr").children()[2]).children().children();
    var name = $("[id*='story-title']", $(this).closest("div")).text();
    if(completed_tasks.length>0){
        var msg = "You cannot delete a user story that has completed tasks.";
        $(".delete_msg").html(msg);            
        $( "#dialog-confirm-many" ).dialog({
            resizable: false,
            height:180,
            modal: true,
            buttons: {
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });   
    }else{
        var msg = "These items will be permanently deleted and cannot be recovered. Are you sure?";
        $(".delete_msg").html(msg);
        $( "#dialog-confirm-many" ).dialog({
            resizable: false,
            height:180,
            modal: true,
            buttons: {
                "Delete item": function() {
                    socket.emit("delete_item", {table:'UserStories', id:story_id, name:name})
                    $( this ).dialog( "close" );
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });   
    }      
  });
  $("#tabs-6 .ui-icon-pencil").click(function(e){
    e.stopPropagation();
    var names = $(this).closest("tr").children()[0];
    var fname = $(names).text().split(" ")[0];
    var lname = $(names).text().split(" ")[1];
    socket.emit("getUser", {FirstName:fname, LastName:lname}, function(data){
       $("#user-dialog").dialog({
                modal : true,
                draggable : false,
                resizable : false,
                width : 800,
                position : {
                        my : "left top",
                        at : "left top",
                        of : ".user-story-header",
                        within : "#tabs-6"
                }
         })
         var form = jQuery("#user-dialog").children("form");
         $("input, textarea, select, select", form).each(function(){
             var name = $(this).attr("name");
             $(this).val(data[0][name]);
         })
         validate_form(form, false);        
    })

  });
}
var setup_dialog = function(dialog, data, within){
    var form = $(dialog).children("form");
    $("input, textarea, select", form).each(function(){
        var name = $(this).attr("name");
        var val = data[0][name];
        $(this).val(val)
    });
    $("[name='isNew']", form).val("false");
    $("[name='id']", form).val(data[0]["id"]);
    $(dialog).dialog({
          modal : true,
          draggable : false,
          resizable : false,
          width : 800,
          position : {
                  my : "left top",
                  at : "left top",
                  of : ".user-story-header",
                  within : within
          }
    })
    validate_form(form, false);
    toggleLoad(false);

}
var add_task_items = function(data, initial){
    if(initial){
    var new_data = new Array();
    var story_arr = new Array();
    for(item in data){
        var this_item = data[item];
        if(story_arr[this_item.UTitle] === undefined){
            story_arr[this_item.UTitle] = new Array();
            story_arr[this_item.UTitle].push(this_item);
        }else{
            story_arr[this_item.UTitle].push(this_item);
        }
        var add_story = this_item.id_Sprints === curr_sprint;
        if(add_story){
          var arr = new Array();
          arr.push(this_item.Title);
          arr.push(this_item.Owner);
          arr.push(this_item.Status);
          arr.push(this_item.PercentComplete);
          arr.push(this_item.Tid);
          new_data.push(arr);
        }
    }
    for(my_story in story_arr){      
      var story = story_arr[my_story];
      var completed = new Array(), in_process = new Array(), to_do = new Array();
      var storyDetail = "", storyId = "", storyPriority = "",  storyHoursEstimated = "", storyPercentComplete = "", storyHoursActual = "", storyNotes = "";
      for(value in story){
          var this_story = story[value];
          storyDetail = this_story.UStory;
          storyId = this_story.Uid;  
          storyPriority = this_story.storyPriority;
          storyHoursEstimated = this_story.storyHoursEstimated;
          storyPercentComplete = this_story.storyPercentComplete;
          storyHoursWorked = this_story.storyHoursWorked;
          storyNotes = this_story.storyNotes;
          var add_story = this_story.id_Sprints === curr_sprint;
          if(add_story){
              var data_string = "<div data-taskid='"+this_story.Tid+"' data-prop='{\"story_id\":\""+this_story.Tid+"\", \"task_id\":\"" + this_story.Tid + "\"}'><div class='story-priority' data-item='Priority'>Priority: <input/><span contentEditable='false'>" +this_story.Priority +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-estimated' data-item='HoursEstimated'>Hours Estimated: <input/><span contentEditable='false'>" +this_story.HoursEstimated +"</span>"+pencil_html+"</div><div class='story-percent-complete' data-item='PercentComplete'>% Complete:  <input/><span contentEditable='false'>" +this_story.PercentComplete +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-worked' data-item='HoursWorked'>Hours Worked:  <input/><span contentEditable='false'>" +this_story.HoursWorked +"</span>"+pencil_html+"</div><div class='story-desc' data-item='Description'>Description:  <textarea></textarea><span contentEditable='false'>" +this_story.Description +"</span>"+pencil_html+"</div></div>";
              data_string = sprint_task_default_html.replace(/DATAHERE/g, data_string).replace(/TITLEHERE/g, this_story.Title);
              if(this_story.Status === "Completed"){
                  completed.push(data_string);
              }else if(this_story.Status === "In Process"){
                  in_process.push(data_string);
              }else if(this_story.Status === "Unclaimed"){
                  to_do.push(data_string);
              }
          }
      }
      var story_div = "<div data-storyid='"+storyId+"'><div>&nbsp;</div><div id='story-id-"+storyId+"' class='hidden'>IDGOHERE</div></div><div class='story-story' data-item='Story'>Story: <textarea></textarea><span contentEditable='false'>" +storyDetail +"</span>"+pencil_html+"</div><div class='story-priority' data-item='Priority'>Priority: <input/><span contentEditable='false'>" +storyPriority +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-estimated' data-item='HoursEstimated'>Hours Estimated: <input/><span contentEditable='false'>" +storyHoursEstimated +"</span>"+pencil_html+"</div><div class='story-percent-complete' data-item='PercentComplete'>% Complete:  <input/><span contentEditable='false'>" +storyPercentComplete +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-worked' data-item='HoursWorked'>Hours Worked:  <input/><span contentEditable='false'>" +storyHoursWorked +"</span>"+pencil_html+"</div><div class='story-desc' data-item='Description'>Description:  <textarea></textarea><span contentEditable='false'>" +storyNotes +"</span>"+pencil_html+"</div></div>";
      var story_info = story_div.replace(/STORYGOHERE/g, this_story.Story).replace(/TITLEGOHERE/g, this_story.UTitle);
      var row_start = "<tr><td class='Story'><div class='accordion-story'>" + story_default_html_sprint.replace(/DATAHERE/g, story_info).replace(/TITLEHERE/g, my_story) + "</div></td>";
      var row_start_hidden = "<tr style='display:none;'><td><div class='accordion-story'>" + story_default_html_sprint.replace(/DATAHERE/g, story_info).replace(/TITLEHERE/g, my_story) + "</div></td>";
      var row_end = "</tr>";
      var new_td = "<td><div class='accordion'>";
      var new_td_ToDo = "<td class='ToDo draggable_sprint'><div class='accordion'>";
      var new_td_InProc = "<td class='InProcess draggable_sprint'><div class='accordion'>";
      var new_td_Comp = "<td class='Completed draggable_sprint'><div class='accordion'>";
      var end_td = "</div></td>";
      var completed_string = "", to_do_string = "", in_process_string = "";
      for(it in to_do){
          to_do_string = to_do_string + to_do[it];
      }
      for(it in in_process){
          in_process_string = in_process_string + in_process[it];
      }
      for(it in completed){
          completed_string = completed_string + completed[it];
      }
        if(completed_string === "" && to_do_string === "" && in_process_string === ""){
          $(".sprint-log-table").append(row_start_hidden + new_td_ToDo+end_td + new_td_InProc+end_td +new_td_Comp+end_td + row_end) 
        }else{
          $(".sprint-log-table").append(row_start + new_td_ToDo+to_do_string +end_td + new_td_InProc+in_process_string +end_td +new_td_Comp+completed_string +end_td + row_end)      
        }          
     
    }
    $("#task_table").dataTable().fnAddData(new_data, true)
    $("#task_table_sprint").dataTable().fnAddData(new_data, true)
    toggleLoad(false);
    }else{
         var arr = new Array();
         arr.push(data.Title);
         arr.push(data.Priority);
         arr.push(data.Status);
         arr.push(data.PercentComplete);
         arr.push(data.Tid);
         var oTable = $("#task_table").dataTable();
         var oTable2 = $("#task_table_sprint").dataTable();
         $("#task_table").dataTable().fnAddData(arr, true)
         $("#task_table_sprint").dataTable().fnAddData(arr, true)
         render_task(data.Tid, data, false, true);   
    }
    accord();
    make_sprint_draggable();
}
var render_task = function(id, data, newid, sprint){
    var this_story = data;
    var story_id = data.id_UserStory;
    if(story_id === undefined){
        story_id = data.Uid;
    }
    var data_string = "<div data-taskid='"+id+"' data-prop='{\"story_id\":\""+story_id+"\", \"task_id\":\"" + id + "\"}'><div class='story-priority' data-item='Priority'>Priority: <input/><span contentEditable='false'>" +this_story.Priority +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-estimated' data-item='HoursEstimated'>Hours Estimated: <input/><span contentEditable='false'>" +this_story.HoursEstimated +"</span>"+pencil_html+"</div><div class='story-percent-complete' data-item='PercentComplete'>% Complete:  <input/><span contentEditable='false'>" +this_story.PercentComplete +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-worked' data-item='HoursWorked'>Hours Worked:  <input/><span contentEditable='false'>" +this_story.HoursWorked +"</span>"+pencil_html+"</div><div class='story-desc' data-item='Description'>Description:  <textarea></textarea><span contentEditable='false'>" +this_story.Description +"</span>"+pencil_html+"</div></div>";
    data_string_sprint = sprint_task_default_html.replace(/REPLACEPENCILHERE/g, pencil_html).replace(/REPLACETRASHHERE/g, trash_html).replace(/REPLACENOTEHERE/g, note_html).replace(/DATAHERE/g, data_string).replace(/TITLEHERE/g, this_story.Title);
    data_string = task_default_html.replace(/REPLACEPENCILHERE/g, pencil_html).replace(/REPLACETRASHHERE/g, trash_html).replace(/REPLACENOTEHERE/g, note_html).replace(/DATAHERE/g, data_string).replace(/TITLEHERE/g, this_story.Title);
    var tdr = 2, td2 = 2;
    if(this_story.Status === "Completed"){
        tdr = 2;
        td2 = 3;
    }else if(this_story.Status === "In Process"){
        tdr = 1;
        td2 = 2;
    }else if(this_story.Status === "Unclaimed"){
        tdr = 1;
        td2 = 1;
    }
    
    if(newid === false){
       $("[data-taskid="+id+"]").each(function(){
         $(this).closest(".ui-accordion").remove(); 
       })
    }
    if(sprint){
        var story_data_sprint = $(".sprint-log-table [data-storyid='"+story_id+"']").closest("tr")
        var row2 = $(story_data_sprint).children()
        $(row2[td2]).children("div").append(data_string_sprint);
        if($(story_data_sprint).css("display") === "none"){
            $(story_data_sprint).attr("style", "")
        }
    }
    var story_data_proj = $(".product-log-table #story-id-"+story_id).closest("tr")
    var row2 = $(story_data_proj).children()
    $(row2[td2]).children("div").append(data_string);
    if($(story_data_proj).css("display") === "none"){
        $(story_data_proj).attr("style", "")
    }        
    var story_data = $(".userstory-log-table #story-id-"+story_id).closest("tr")
    var row = $(story_data).children()
    if($(story_data).css("display") === "none"){
        $(story_data).attr("style", "")
    }
    $(row[tdr]).children("div").append(data_string);
}
var make_sprint_draggable = function(){
    $( ".ToDo .ui-accordion, .InProcess .ui-accordion, .Completed .ui-accordion" ).draggable({
        appendTo: "body",
        helper:"clone",
        create: function(event, ui){
            $(this).draggable("option", "containment", $(this).closest("tr"));
        }
    });
    $( ".draggable_sprint" ).droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function( event, ui ) {
            var old_parent = $(ui.draggable).parent();
            var old_css_parent = $(ui.draggable).closest("td").attr("class");
            $(".accordion", this ).append(ui.draggable);
            var parent = $(ui.draggable).closest("td").attr("class");
            var id = $("[data-taskid]", ui.draggable).attr("data-taskid");
            var hours = "";
            var cancel = false;
            if(parent === old_css_parent){
                return;
            }
            if(parent.indexOf("Complete")>-1){
                $(".hours-dialog").dialog({
                    buttons:{"Enter":function(){
                            hours = $(".hours-dialog [name='HoursWorked']").val();
                            $(".hours-dialog").dialog("close");
                            var data = {id:id, parent:parent, Owner:$.cookie("name"), HoursWorked:hours};
                            socket.emit("draggedSprintTask", data);
                        },
                        "Cancel":function(){
                            cancel = true;
                            $(old_parent).append(ui.draggable);
                            $(".hours-dialog").dialog("close");
                        }
                    }
                    
                })
            }else{                
                var data = {id:id, parent:parent, Owner:$.cookie("name"), HoursWorked:hours}
                socket.emit("draggedSprintTask", data);
            }
            if(cancel){return;}
        }
    })
}
$('[data-item]').live('dblclick',
    function(e){
        e.stopPropagation();
        $(this).children("span").hide();
        $(this).children("input, textarea").show();
        $(this).children("input, textarea").focus();
        $(this).children("input, textarea").select();
        $(this).children("input, textarea").val($(this).children("span").text());
        $(this).children(".ui-icon-pencil").remove();
    }
);
$('.InProcess [data-item], .ToDo [data-item], .Completed [data-item]').live('blur',
    function(e){
        $(this).children("span").show();
        $(this).children("input, textarea").hide();
        var data_item = $(this).attr("data-item");
        var id = $(this).parent().attr("data-taskid");
        $("[data-taskid='"+id+"']").children("[data-item='"+data_item+"']").children("span").html($(this).children("input, textarea").val())
        $(this).append(pencil_html);
        var res = {id:id, ret:{"Tasks":[{column:data_item, value:$(this).children("input, textarea").val()}]}, isUpdate:true};                
        socket.emit("update_existing_item", res, function(a, task_id, b){
            //nada
        })
    }
);
$('.Story [data-item]').live('blur',
    function(e){
        $(this).children("span").show();
        $(this).children("input, textarea").hide();
        var data_item = $(this).attr("data-item");
        var id = $(this).closest(".inner-task-data").children("[data-storyid]").attr("data-storyid");
        $("[data-storyid='"+id+"']").parent().children("[data-item='"+data_item+"']").children("span").html($(this).children("input, textarea").val())
        $(this).append(pencil_html);
        var res = {id:id, ret:{"UserStories":[{column:data_item, value:$(this).children("input, textarea").val()}]}, isUpdate:true}; 
        socket.emit("update_existing_item", res, function(a, task_id, b){
            //nada
        })
    }
);