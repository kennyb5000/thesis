var task_default_html = '<div class="acChild"><h3 class="task-data">REPLACETRASHHERE<div class="title">TITLEHERE</div></h3><div class="inner-task-data">DATAHERE</div></div>';
var story_default_html = '<div class="acChild"><h3 class="story-data">REPLACETRASHHERE<div class="title">TITLEHERE</div></h3><div class="inner-task-data">DATAHERE</div></div>';
var story_page_default_html = '<div class="acChild"><h3 class="story-data">REPLACETRASHHEREREPLACENOTEHERE<div class="title">TITLEHERE</div></h3><div class="inner-task-data">DATAHERE</div></div>';
var sprint_task_default_html = '<div class="acChild"><h3 class="task-data"><div class="title">TITLEHERE</div></h3><div class="inner-task-data">DATAHERE</div></div>';
var story_default_html_sprint = '<div class="acChild"><h3 class="story-data"><div class="title">TITLEHERE</div></h3><div class="inner-task-data">DATAHERE</div></div>';
var pencil_html = '<div class="ui-icon ui-corner-all ui-icon-pencil icon right-floater" ></div>';
var trash_html = '<div class="ui-icon icon ui-corner-all ui-icon-trash right-floater"></div>';
var note_html = '<div class="ui-icon ui-corner-all ui-icon-note icon right-floater" ></div>';
var socket_init = function(){
    
    socket.on('disconnect', function() {
        $("#message").append("<div>Connection was lost with the Host.  \nThe page will be reloaded once connection is reestablished.</div>")
            .css({"position":"absolute", "left":"45%", "top":"45%", "width":"250px", "z-index":"5000", "background-color":"white"})
        $("#loading").show();
        var connection = false;
        var test_connection = function(){
          socket.emit("testConnection", "", function(){
              connection = true;
          }) 
          if(connection === false){
            var timeout = setTimeout(function(){test_connection()}, 1000);
          }else{
              window.location.reload();
          }
        }
        var timeout = setTimeout(function(){test_connection()}, 1000);
    });
    socket.emit("connected", $.cookie("name"));
    socket.on("chat_users", function(data){
        console.log(data)
        for(value in data){
            console.log(value)
            $(".chat_users ul").append("<li>"+value+"</li>");
            $(".num_chat_users").html(parseInt($(".num_chat_users").html())+1);
        }
    })
    socket.on("msg", function(data){
        console.log(data)
        $(".chat_posts").append("<div class='theirpost'><div class='msg inline triangle-right right'>"+data.msg.replace(/\n/g, "<br/>")+": </div><div class='user inline'>"+data.user+"</div></div>");
        $(".num_new").html(parseInt($(".num_new").html())+1);
    })
    socket.on("sysmsg", function(data){
        console.log(data)
        $(".chat_posts").append("<div class='systempost'><div class='msg inline'>"+data.msg.replace(/\n/g, "<br/>")+"</div></div>");
        $(".num_new").html(parseInt($(".num_new").html())+1);
    })
    socket.on("newChatUser", function(data){
        $(".chat_users ul").append("<li>"+data+"</li>");
        $(".num_chat_users").html(parseInt($(".num_chat_users").html())+1);
    })
    socket.on("setChart", function(data){
        var i = 1;
         names = new Array();
        names.push([0, 0])
         items = new Array();
        items.push(null)
        for(value in data){
            var this_item = data[value];
            items.push([i,parseFloat(this_item.HoursEstimated) - parseFloat(this_item.HoursWorked)])
            names[i] = [i, this_item.Date];
            i++;
        }
        
        $.plot("#chartdiv", [{label: "BurnDown for Sprint ", data:items}], {
        series: {

            lines: {
                show: true
            },
            points: {
                show: true
            }
        }, 
        grid: {

            hoverable: true,
            clickable: true,
            margin: {
                top: 25,
                left: 25,
                bottom: 25,
                right: 25
            },
            minBorderMargin:25 
        },
        xaxis: {
             ticks: names,
             label: "Date"
           }

        }); 
        
        function showTooltip(x, y, contents) {
            $("<div id='tooltip'>" + contents + "</div>").css({
                position: "absolute",
                display: "block",
                top: y + 5,
                left: x + 5,
                border: "1px solid #fdd",
                padding: "2px",
                "background-color": "#fee",
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        } 
        $("#chartdiv").bind("plothover", function (event, pos, item) {
            $("#tooltip").each(function(){$(this).remove()})
            if (item) {
                showTooltip(pos.x+pos.pageX, pos.y+pos.pageY, names[item.datapoint[0]][1] + " : " + item.datapoint[1])
            }
        });
    })
    socket.on("dragCompleted", function(data){
        var id = data.item, parent=data.parent;
        var item = $("#tabs-3 [data-taskid='"+id+"']").parent().parent("div");
        var item2 = $("#tabs-4 [data-taskid='"+id+"']").parent().parent("div");
        var item3 = $("#tabs-5 [data-taskid='"+id+"']").parent().parent("div");
        var par = $(item).closest("tr");
        var par2 = $(item2).closest("tr");
        var par3 = $(item3).closest("tr");
        if(parent.indexOf("Complete")>-1){
            $(".story-percent-complete span", item).html(data.data.PercentComplete);
            $(".story-hours-worked span", item).html(data.data.HoursWorked);
            $(".story-percent-complete span", item2).html(data.data.PercentComplete);
            $(".story-hours-worked span", item2).html(data.data.HoursWorked);
            $(".story-percent-complete span", item3).html(data.data.PercentComplete);
            $(".story-hours-worked span", item3).html(data.data.HoursWorked);
        }           
        $(par).children("."+parent.split(" ")[0]).children(".accordion").append(item);
        $(par2).children("."+parent.split(" ")[0]).children(".accordion").append(item2);
        $(par3).children("."+parent.split(" ")[0]).children(".accordion").append(item3);
    })
    socket.on("newUser", function(data){
        var newArr = new Array();
        newArr.push(data.Name);
        newArr.push(data.Role);
        newArr.push(data.Email);
        newArr.push(trash_html + pencil_html);
        $('#manage_table_id').dataTable().fnAddData(newArr);
    })
    socket.on("renderUsers", function(data){
        var users = new Array();
        for(item in data){
            var this_item = data[item];
            var newArr = new Array();
            newArr.push(this_item.Name);
            newArr.push(this_item.Role);
            newArr.push(this_item.Email);
            newArr.push(trash_html + pencil_html);
            users.push(newArr);
        }
        $('#manage_table_id').dataTable().fnAddData(users);
        $('#manage_table_id').dataTable().fnSetColumnVis(3, false);
        var role = $.cookie("role");
        if(role === "Owner" || role === "Project Owner" || role === "Scrum Master"){
            $('#manage_table_id').dataTable().fnSetColumnVis(3, true);
        }
           
    })
    socket.on("new_rss", function(data){
        var default_html = "<li>"+data.msg+" on <div style='margin-left:15px;'>" + $.fullCalendar.formatDate(new Date(data.time), "ddd MMMM ddS hh:mm tt")+"</div></li>";
        var i = default_html;
        var p = 0;
        $("ul.rss").trigger("insertItem", [i,p]);
        //$("ul.rss").append(default_html)
    })
    socket.on("renderRSS", function(data){
        for(item in data){
            var default_html = "<li>"+data[item].event+" on <div style='margin-left:15px;'>" + $.fullCalendar.formatDate(new Date(parseInt(data[item].timestamp)), "ddd MMMM ddS hh:mm tt") + "</div></li>";
            $("ul.rss").prepend(default_html);
        }
        var start = $("ul.rss").children().length-1;
        $("ul.rss").carouFredSel({
            direction: "up",
            circular: false,
            infinite: false,
            height: 150,
            width: 400,
            items: {
                visible: 5,
                minimum: 1,
                width: "350px",
                start: 0
            },
            auto: false,
            prev    : {
	        button  : "#list_prev",
	        key     : "left"
	    },
	    next    : {
	        button  : "#list_next",
	        key     : "right"
	    },
            padding: [0,15,0,0]
        });
    })
    socket.on("mod_calendar", function(data){
        if(data.newItem){
            data.data["id"] = "cal_"+parseInt(data.id);
            data.data["start"] = $.fullCalendar.parseDate( data.data["start"] )
            data.data["end"] = $.fullCalendar.parseDate( data.data["end"] )
            calendar.fullCalendar('renderEvent',data.data,true)
        }else{
            data.data["id"] = "cal_"+parseInt(data.id);
            data.data["start"] = $.fullCalendar.parseDate( data.data["start"] )
            data.data["end"] = $.fullCalendar.parseDate( data.data["end"] )
            calendar.fullCalendar( 'removeEvents' , "cal_"+parseInt(data.id) )
            calendar.fullCalendar('renderEvent',data.data,true)
        }
    })
    socket.on("renderCalendar", function(data){
        var calendar = $('#calendar');
        for(item in data){
            data[item]["id"] = "cal_" + data[item]["id"];
            if(data[item]["allDay"] === "false"){
                data[item]["allDay"] = false;
            }else{
                data[item]["allDay"] = true;
            }
            calendar.fullCalendar('renderEvent',data[item],true)
        }
    })
    socket.on("sync_tasks", function(data){
        if(data.isUpdate){
            var attr = "";
            for(val in data.data){
                attr = val;
            }
            $("[data-taskid='"+data.id+"']").children("[data-item='"+attr+"']").children("span").html(data.data[attr])
            return;
        }
        var table = data.table;
        var id = data.id;
        var data_new = data.data;
        add_inits[table](id, data_new, data.isNew);
    })
    socket.on("delete_item", function(data){
        var table = data.table;
        var id = data.id;
        del_inits[table](id);
    })
    socket.on('set_project_data', function (data) {
      for(item in data){
          var this_item = data[item];
          var title = this_item.Name;
          var startDate = this_item.startDate;
          var endDate = this_item.EstimatedCompletionDate;
          var desc = this_item.Notes;
          var days_remaining = Math.round((new Date(endDate)-new Date())/(1000*60*60*24)-1);
          $("#home-project-name h3").html("Project - " + title);
          $("#home-days-remaining").html(days_remaining);
          $("[name='home-view-details']").val(desc);
      }
    });
    socket.on('add_task_table_items', function (data) {
      add_task_items(data, true)      
    });  
    socket.on('add_project_table_items', function (data) {
      var story_arr = new Array();
      for(item in data){
          var this_item = data[item];
          if(story_arr[this_item.UTitle] === undefined){
              story_arr[this_item.UTitle] = new Array();
              story_arr[this_item.UTitle].push(this_item);
          }else{
              story_arr[this_item.UTitle].push(this_item);
          }
      }
      for(my_story in story_arr){
        var story = story_arr[my_story];
        var completed = new Array(), in_process = new Array(), to_do = new Array();
        var storyDetail = "", storyId = "", storyPriority = "",  storyHoursEstimated = "", storyPercentComplete = "", storyHoursActual = "", storyNotes = "";
        for(value in story){
            var this_story = story[value];
            storyDetail = this_story.UStory;
            storyId = this_story.Uid;  
            storyPriority = this_story.storyPriority;
            storyHoursEstimated = this_story.storyHoursEstimated;
            storyPercentComplete = this_story.storyPercentComplete;
            storyHoursWorked = this_story.storyHoursWorked;
            storyNotes = this_story.storyNotes;
            var taskId = this_story.Tid; 
            var data_string = "<div data-taskid='"+taskId+"' data-prop='{\"story_id\":\""+storyId+"\", \"task_id\":\"" + taskId + "\"}'><div class='story-priority' data-item='Priority'>Priority: <input/><span contentEditable='false'>" +this_story.Priority +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-estimated' data-item='HoursEstimated'>Hours Estimated: <input/><span contentEditable='false'>" +this_story.HoursEstimated +"</span>"+pencil_html+"</div><div class='story-percent-complete' data-item='PercentComplete'>% Complete:  <input/><span contentEditable='false'>" +this_story.PercentComplete +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-worked' data-item='HoursWorked'>Hours Worked:  <input/><span contentEditable='false'>" +this_story.HoursWorked +"</span>"+pencil_html+"</div><div class='story-desc' data-item='Description'>Description:  <textarea></textarea><span contentEditable='false'>" +this_story.Description +"</span>"+pencil_html+"</div></div>";
            data_string = task_default_html.replace(/REPLACEPENCILHERE/g, pencil_html).replace(/REPLACETRASHHERE/g, trash_html).replace(/REPLACENOTEHERE/g, note_html).replace(/DATAHERE/g, data_string).replace(/TITLEHERE/g, this_story.Title);
            if(this_story.Status === "Completed"){
                completed.push(data_string);
            }else if(this_story.Status === "In Process"){
                in_process.push(data_string);
            }else if(this_story.Status === "Unclaimed"){
                to_do.push(data_string);
            }
        }
        var story_div = "<div data-storyid='"+storyId+"'><div>&nbsp;</div><div id='story-id-"+storyId+"' class='hidden'>IDGOHERE</div></div><div class='story-story' data-item='Story'>Story: <textarea></textarea><span contentEditable='false'>" +storyDetail +"</span>"+pencil_html+"</div><div class='story-priority' data-item='Priority'>Priority: <input/><span contentEditable='false'>" +storyPriority +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-estimated' data-item='HoursEstimated'>Hours Estimated: <input/><span contentEditable='false'>" +storyHoursEstimated +"</span>"+pencil_html+"</div><div class='story-percent-complete' data-item='PercentComplete'>% Complete:  <input/><span contentEditable='false'>" +storyPercentComplete +"</span>"+pencil_html+"</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='story-hours-worked' data-item='HoursWorked'>Hours Worked:  <input/><span contentEditable='false'>" +storyHoursWorked +"</span>"+pencil_html+"</div><div class='story-desc' data-item='Description'>Description:  <textarea></textarea><span contentEditable='false'>" +storyNotes +"</span>"+pencil_html+"</div></div>";
        var story_info = story_div.replace(/STORYGOHERE/g, storyDetail).replace(/TITLEGOHERE/g, my_story).replace(/IDGOHERE/g, storyId);
        var row_start = "<tr><td class='Story'><div class='accordion-story'>" + story_page_default_html.replace(/REPLACETRASHHERE/g, trash_html).replace(/REPLACENOTEHERE/g, note_html).replace(/DATAHERE/g, story_info).replace(/TITLEHERE/g, my_story) + "</div></td>";
        var row_start_hidden = "<tr style='display:none'><td><div class='accordion-story'>" + story_page_default_html.replace(/REPLACETRASHHERE/g, trash_html).replace(/REPLACENOTEHERE/g, note_html).replace(/DATAHERE/g, story_info).replace(/TITLEHERE/g, my_story) + "</div></td>";
        var row_end = "</tr>";
        var new_td = "<td><div class='accordion'>";
        var new_td_ToDo = "<td class='ToDo'><div class='accordion'>";
        var new_td_InProc = "<td class='InProcess'><div class='accordion'>";
        var new_td_Comp = "<td class='Completed'><div class='accordion'>";
        var new_td_notComp = "<td class='InProcess ToDo'><div class='accordion'>";
        var end_td = "</div></td>";
        var completed_string = "", to_do_string = "", in_process_string = "";
        for(it in to_do){
            to_do_string = to_do_string + to_do[it];
        }
        for(it in in_process){
            in_process_string = in_process_string + in_process[it];
        }
        for(it in completed){
            completed_string = completed_string + completed[it];
        }
        if(to_do.length === 0 && in_process.length === 0 && completed.length === 0){
          $(".product-log-table").append(row_start_hidden + new_td_ToDo+end_td + new_td_InProc+end_td +new_td_Comp+end_td + row_end)
        }else{
            $(".product-log-table").append(row_start + new_td_ToDo+to_do_string +end_td + new_td_InProc+in_process_string +end_td +new_td_Comp+completed_string +end_td + row_end)
        }
        $(".userstory-log-table").append(row_start + new_td_notComp+to_do_string + in_process_string +end_td +new_td_Comp+completed_string +end_td + row_end)
             
      }
      accord();
   });
     socket.on('sprint_defaults', function (data) {
      for(item in data){
          var this_item = data[item];
          var title = this_item.Name;
          var startDate = this_item.StartDate;
          var endDate = this_item.EndDate;
          var desc = this_item.Description;
          var days_remaining = Math.round((new Date(endDate)-new Date())/(1000*60*60*24)-1);
          $("#sprint-project-name h3").html("Sprint - " + title);
          $("#sprint-days-remaining").html(days_remaining);
          $("[name='sprint-view-details']").val(desc);
      }
     });
     socket.on('all_sprints', function (data) {
      for(item in data){
          var this_item = data[item];
      }
      function SortByDate(a, b){
        var aDate = new Date(a.StartDate).setHours(0,0,0,0);
        var bDate = new Date(b.StartDate).setHours(0,0,0,0); 
        return ((aDate < bDate) ? -1 : ((aDate > bDate) ? 1 : 0));
      }
      var sprint_string = '<dd data-sprintid="REPLACEIDHERE" class="existing-sprint clearfix"><div class="left-floater wrapper sprint-name-list">SPRINTNAMEHERE<div style="display:none;">SPRINTIDHERE</div></div>'+trash_html+pencil_html+'</dd>';
      data.sort(SortByDate);
      for(item in data){
          var this_item = data[item];
          var calendar = $('#calendar');
          var id = this_item.id;
          var startDate = this_item.StartDate;
          var endDate = this_item.EndDate;
          var name = this_item.Name;
          var today = new Date().setHours(0,0,0,0);
          if(new Date(startDate).setHours(0,0,0,0) <= today && new Date(endDate).setHours(0,0,0,0) >= today){
              curr_sprint = this_item.id;
              var desc = this_item.Description;
              var days_remaining = Math.round((new Date(endDate)-new Date())/(1000*60*60*24)-1);
              $("#sprint-project-name h3").html("Sprint - " + name);
              $("#sprint-days-remaining").html(days_remaining);
              $("[name='sprint-view-details']").val(desc);
          }
          var new_sprint_string = sprint_string.replace(/REPLACEIDHERE/g, id).replace(/SPRINTNAMEHERE/g, name+": ").replace(/SPRINTIDHERE/g, id);
          $(".add-sprint").before(new_sprint_string);
          var event = {id:"sprint_"+id, title:name, start:new Date(startDate), end:new Date(endDate), editable:false};
          calendar.fullCalendar("renderEvent", event, true)
      }
      
      //SPRINT CLICK EDIT
       $("#tabs-2 .existing-sprint .ui-icon-pencil").live("click",function(){
        toggleLoad(true);
        var task_id = $(this).closest(".existing-sprint").find("div").children().text();
        socket.emit("get_info", {table:'Sprints', id:task_id}, function(data){
            $("[name='isNew']", form).val("true");
            $(".datepicker-sprint").datepicker();
            $("#dialog-sprint").dialog({
                    modal : true,
                    draggable : false,
                    resizable : false,
                    width : 800,
                    position : {
                            my : "left top",
                            at : "left top",
                            of : "#sprint-project-name",
                            within : "#tabs-2"
                    }
            })
            var form = $("#dialog-sprint form");
            $("input, textarea, select", form).each(function(){
               $(this).val(data[0][$(this).attr("name")]); 
            })
            $("[name='isNew']", form).val("false");
            socket.emit("getBacklogItemsSpecific", data[0].id, function(data){
                var sprint = data.id;
                var data = data.data;
                var story_default_html = "<div class='portlet_topper ui-corner-all'>DATAHERE<span class='ui-icon ui-icon-minusthick'></span></div>";
                var new_data = new Array();
                var story_arr = new Array();
                for(item in data){
                    var this_item = data[item];
                    uid = this_item.Uid;
                    var data_string = "<div data-all='" + JSON.stringify(this_item) + "' data-sprint='ADDSPRINTDATA' data-taskid='"+this_item.Tid+"'><div class='story-title'>" +this_item.Title +"</div><div class='story-priority'>" +this_item.Priority +"</div><div class='story-percent-complete'>" +this_item.PercentComplete +"</div></div>";
                    if(this_item.id_Sprints !== "BLANK" && this_item.id_Sprints !== "" && this_item.id_Sprints !== null){
                          data_string = data_string.replace(/ADDSPRINTDATA/g, "1")
                      }else{
                          data_string = data_string.replace(/ADDSPRINTDATA/g, "0")
                      }
                    data_string = sprint_task_default_html.replace(/DATAHERE/g, data_string).replace(/TITLEHERE/g, this_item.Title);    
                    if(this_item.id_Sprints !== "BLANK" && this_item.id_Sprints !== "" && this_item.id_Sprints !== null){
                          $("#sprint_backlog_target .column").append(data_string);
                      }else{
                          $("#product_backlog_source .column").append(data_string);
                      }
                    
                    $( ".column > div.acChild" ).accordion({
                        collapsible: true,
                        active:false
                    });
                }
                $( ".column" ).sortable({
                  connectWith: ".column",
                  items:".acChild",
                  helper: "clone",
                  placeholder: "ui-state-highlight",
                  containment: ".portlet-content-main",
                  stop: function(event, ui){
                      $(".portlet-content-main").equalHeights();                      
                  }
                });                  
                $(".portlet-content-main").equalHeights();
                $( ".task-content" ).disableSelection();
                
            toggleLoad(false);
         })
     });
     });
    });
    validate_form = function(form, validate){
        var items = new Array();
        jQuery("input, select, textarea", form).each(function(){
            var this_item = $(this);
            if(this_item[0].attributes["data-prop"]){
                var content = new Array();
                var jsons = JSON.parse(this_item[0].attributes["data-prop"].nodeValue);
                if(jsons.data_type === "number" && (isNaN($(this).val()) || ($(this).val() === "" && (jsons.required === "true" || validate === false)))){
                  content.push("must be a number");
                }
                if(jsons.required && this_item.val() === ""){
                  content.push("is required");
                }
                if(content.length > 0){
                    $(this_item).tooltip({
                        items:"input, textarea, select, select",
                        content:function(){
                            return "This field " + content.join(" and ") + "!";
                        },
                        position:{
                            my:"left",
                            at:"right"
                        }
                    })
                    if(validate){
                        $(this).tooltip("open")
                    }
                    items.push("this");
                }
            }
            
        });
        if(items.length > 0){
          return false;
        }else{
          return true;
        }
    }
}