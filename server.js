var express = require('express'), everyauthRoot = __dirname + '/..', Q = require('q');
var http = require('http'), passport = require('passport'), LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
var cronJob = require('cron').CronJob;
var crypto = require('crypto')
, key = 'scrummgmttoolistoocoolforschool';
var User = {};
var springbase = require('springbase');
var conn = new springbase.data.Connection({ 
   application: "1ow7sXbF_1vQ",
   username: "rosebudreb@yahoo.com",
   password: "p0t4t0s"
});
var regen_users = function(){
  var user_query = conn.getQuery("getUsers");
  user_query.on("ready", function(){
      var reader  = user_query.execute();
      reader.on("ready", function(){
          User.users=reader.readAll();
      })
  })
}
regen_users();
User.findOne = function(username, callback){
    for(user in User.users){
        var this_user = User.users[user];
            
        if(this_user.Username === username.Username){
            callback("", this_user);
            return;
        }
    }
    callback("", false);
    return;
}
User.validPassword = function(psword, user){
    if(user.Password !== psword){
        return false;
    }
    return true;
}
User.findById = function(id, callback){
    for(user in User.users){
        var this_user = User.users[user];
        if(this_user.id === id){
            callback("", this_user);
            return;
        }
    }
    callback("", false);
    return;
}
User.findRole = function(username){
    for(user in User.users){
        var this_user = User.users[user];
        if(this_user.Username === username){
            return this_user.Role;
        }
    }
    return "";
}
passport.use(new LocalStrategy(
  function(username, password, done) {
    var crypto = require('crypto')
	, key = 'scrummgmttoolistoocoolforschool';
    var cipher = crypto.createCipher('aes-256-cbc', key)
    decipher = crypto.createDecipher('aes-256-cbc', key);
	cipher.update(password, 'utf8', 'base64');
    password = cipher.final('base64') 
    User.findOne({ Username: username }, function (err, user) {
      if (err !== "") { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!User.validPassword(password, user)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});
var app = express();
var server = http.createServer(app);
app.configure(function () {
	app.use(express.bodyParser());
	app.use(express.static(__dirname + "/public"));
	app.use(express.favicon());
	app.use(express.cookieParser());
	app.use(express.session({
			secret : 'supercoolpotatoes'
		}));
                
        app.use(passport.initialize());
        app.use(passport.session());
	app.set('view engine', 'jade');
	app.set('views', __dirname + '/views');
	app.use(express.errorHandler());
        app.use(flash());
        app.engine('html', require('ejs').renderFile);
});

app.post('/login',
  passport.authenticate('local', { successRedirect: '/projects',
                                   failureRedirect: '/projects',
                                   failureFlash: true }));
app.get('/', function(req, res){
    var flash = req.flash();
    
    if(flash.length>0){
        res.render("login.ejs",{message: flash.error[0]});
    }else{
        res.render("login.ejs",{message: ""});
    }
})
app.get('/projects', function (req, res) {
	
        if(req.user === undefined){
          return res.redirect('/');
        }else{
            res.cookie("role", req.user.Role, {domain:'localhost', path:""})
            res.cookie("name", req.user.Username, {domain:'localhost', path:""})
            res.render('mainpage.html');
        }
});
app.get('/logout', function(req, res) {
    req.logOut();
    res.redirect('/');
});
var io = require('socket.io').listen(server);

  
var curr_sprint = '3', curr_project = '1';
var chat_users = [];
io.sockets.on('connection', function (socket) {
  socket.on("connected", function(data){
      socket.broadcast.emit("sysmsg", {msg:data + " joied the room.", user:data});
      socket.emit("chat_users", chat_users);
      console.log(chat_users)
      chat_users[data] = data;
      console.log(chat_users)
      socket.broadcast.emit("newChatUser", data);
  })
  socket.on("msg", function(data){
      socket.broadcast.emit("msg", {msg:data.msg, user:data.user});
  })
  var getSprintData = function(sprint){
    var querys = conn.getQuery("getTasksInCurSprint");
      querys.on("ready", function() {
        var reader = querys.execute({sprint_id:sprint});
        var data = new Array();
        reader.on("ready", function() {
          data = reader.readAll();
          socket.emit("add_task_table_items", data);
        });
    });  
  }
  var sprint_defer = Q.defer(), sprint_promise = sprint_defer.promise;
  var project_defer = Q.defer(), project_promise = project_defer.promise;
  var query2 = conn.getQuery("getAllProjects");
  query2.on("ready", function() {
    var reader = query2.execute();
    var data = new Array();
    reader.on("ready", function() {
      data = reader.readAll();
      for(item in data){
          var this_item = data[item];
          curr_project = this_item.id;
      }
      project_defer.resolve(curr_project);
      socket.emit("set_project_data", data);
    });
  }); 
  project_promise.then(function(project){      
    var cal_query = conn.getQuery("getCalendarItems");
    cal_query.on("ready", function(){
        var reader  = cal_query.execute();
        reader.on("ready", function(){
            var data=reader.readAll();
            socket.emit("renderCalendar", data);
        })
    })  
    var user_query = conn.getQuery("getProjectUsers");
    user_query.on("ready", function(){
        var reader  = user_query.execute();
        reader.on("ready", function(){
            var data=reader.readAll();
            socket.emit("renderUsers", data);
        })
    })
    var rss_query = conn.getQuery("getRSSFeed");
    rss_query.on("ready", function(){
        var reader  = rss_query.execute();
        reader.on("ready", function(){
            var data=reader.readAll();
            socket.emit("renderRSS", data);
        })
    })  
    var all_sprint_data_query = conn.getQuery("getAllSprints");
    all_sprint_data_query.on("ready", function(){
        var reader = all_sprint_data_query.execute({project_id:project});
        reader.on("ready",function(){
            socket.emit("all_sprints", reader.readAll());
          var data = reader.readAll();
          for(item in data){
            var this_item = data[item];
            var startDate = this_item.StartDate;
            var endDate = this_item.EndDate;
            var name = this_item.Name;
            var today = new Date().setHours(0,0,0,0);
            if(new Date(startDate).setHours(0,0,0,0) <= today && new Date(endDate).setHours(0,0,0,0) >= today){
                curr_sprint = this_item.id;
                sprint_defer.resolve(curr_sprint);
                
                var query2 = conn.getQuery("getBurnDownForSprint");
                query2.on("ready", function() {
                  var reader = query2.execute({sprint_id:curr_sprint});
                  var data = new Array();
                  reader.on("ready", function() {
                    data = reader.readAll();
                  
                    socket.emit("setChart", data);
                  });
                }); 
            }
          }
        })
    })
    sprint_promise.then(function(sprint){           
        getSprintData(sprint)
        var project_querys = conn.getQuery("getTasksInCurProject");
        project_querys.on("ready", function() {
          var reader = project_querys.execute({project_id:project+""});
          var data = new Array();
          reader.on("ready", function() {
            data = reader.readAll();
            socket.emit("add_project_table_items", data);
          });
        });  
    })
  });
  socket.on("draggedSprintTask", function(data){
    
    var task_querys = conn.getQuery("getTask");
    var Owner = data.Owner;
    var Status = "Unclaimed";
    var PercentComplete = "";
    if(data.parent.indexOf("ToDo")>-1){
        Owner = "None";
        Status = "Unclaimed";
    }else if(data.parent.indexOf("InProcess")>-1){
        Owner = data.Owner;
        Status = "In Process";
    }else if(data.parent.indexOf("Complete")>-1){
        Owner = data.Owner;
        Status = "Complete";
    }
    var data2 = {};
    data2.Owner = Owner;
    data2.Status = Status;
    if(data.parent.indexOf("Complete")>-1){
      PercentComplete = "100";
      data2.PercentComplete = PercentComplete;
      data2.HoursWorked = data.HoursWorked;
    }
    
    var table_test = conn.getTable("Tasks");
    table_test.on("ready", function(){
        table_test.updateRow(parseInt(data.id), data2)
    })
    socket.emit("dragCompleted", {item:data.id, parent:data.parent, data:data2});
    socket.broadcast.emit("dragCompleted", {item:data.id, parent:data.parent, data:data2});
  });
  socket.on("setName", function(name){
    socket.set("nickname", name, function(){
        socket.emit("ready");
    })
  })
  socket.on("getUser", function(data, callback){
    var user_querys = conn.getQuery("getUser");
    user_querys.on("ready", function() {
      var reader = user_querys.execute({first:data.FirstName, last:data.LastName});
      var data2 = new Array();
      reader.on("ready", function() {
        var data2 = reader.readAll();
        callback(data2);
      });
    }); 
  })
  socket.on("redo_sprint", function(id){
    getSprintData(id);
  })
  name_sets = {Sprints:"Name", UserStories:"Title", Tasks:"Title", Calendar:"title"}
        
  socket.on("add_new_item", function(data, fn){
    var result = {};
    var needing_fb = new Array("UserStories");
    var needing_project = new Array("Sprints");
    var needing_blank = new Array("Tasks");
    for(table in data){
        for(column in data[table]){
            var this_col = data[table][column];
            var columns = this_col.column;
            if(this_col["value"] === ''){
                this_col["value"] = null;
            }
            result[columns] = this_col["value"];
        }
        if(needing_fb.indexOf(table) > -1){
            result["id_FeatureBacklog"] = curr_project;
        }
        if(needing_project.indexOf(table) > -1){
            result["id_ProjectDetail"] = curr_project;
        }
        if(needing_blank.indexOf(table) > -1){
            result["id_Sprints"] = "BLANK";
        }
        var table_base = conn.getTable(table);
        var msg = "Created new " + table + " with " + name_sets[table] + " of " + result[name_sets[table]];
        table_base.on("ready", function(){
            table_base.insertRows(result, function(data){
                var itemID = parseInt(data);
                fn(table, itemID, result);
                socket.broadcast.emit("sync_tasks", {table:table, id:itemID, data:result, isNew:true})
            });
        })
        var table_rss = conn.getTable("RSS");
        table_rss.on("ready", function(){
            var timestamp = new Date().getTime();
            var data = {event:msg, timestamp:timestamp}
            table_rss.insertRows(data, function(data){ 
                var itemID = parseInt(data);
            });
            socket.broadcast.emit("new_rss", {msg:msg, time:timestamp});
            socket.emit("new_rss", {msg:msg, time:timestamp});
        })
    }
  })
  socket.on("update_existing_item", function(data, fn){
    var result = {};
    for(table in data.ret){
        var id = parseInt(data.id);
        for(column in data.ret[table]){
            var this_col = data.ret[table][column];
            var columns = this_col.column;
            if(this_col["value"] === ''){
                this_col["value"] = null;
            }
            result[columns] = this_col["value"];
        }
        var table_base = conn.getTable(table);
        var msg = "Updated " + table + " with " + name_sets[table] + " of " + result[name_sets[table]];
        table_base.on("ready", function(){
            table_base.updateRow(id,result);
            fn(table, id, result, data.data_all);
            socket.broadcast.emit("sync_tasks", {table:table, id:id, data:result, isNew:false, isUpdate:data.isUpdate})
        })
        var table_rss = conn.getTable("RSS");
        table_rss.on("ready", function(){
            var timestamp = new Date().getTime();
            var data = {event:msg, timestamp:timestamp}
            table_rss.insertRows(data, function(data){ 
                var itemID = parseInt(data);
            });
            socket.broadcast.emit("new_rss", {msg:msg, time:timestamp});
            socket.emit("new_rss", {msg:msg, time:timestamp});
        }) 
    }
  })
  socket.on("delete_item", function(data){
      var id = parseInt(data.id);
      var table = data.table;
      var name = data.name;
      var table_base = conn.getTable(table);
      var msg = "Deleted " + table + " item with " + name_sets[table] + " of " + name;
      table_base.on("ready", function(){
        table_base.deleteRows(id);
        socket.broadcast.emit("delete_item", {table:table, id:id});
        socket.emit("delete_item", {table:table, id:id});
      })
      var table_rss = conn.getTable("RSS");
      table_rss.on("ready", function(){
        var timestamp = new Date().getTime();
        var data = {event:msg, timestamp:timestamp}
        table_rss.insertRows(data, function(data){ 
            //nada
        });
        socket.broadcast.emit("new_rss", {msg:msg, time:timestamp});
        socket.emit("new_rss", {msg:msg, time:timestamp});
      })
  })
  socket.on("update_tasks", function(data){
    var table_base = conn.getTable("Tasks");
        table_base.on("ready", function(){
            table_base.updateRow(data.id,{"Title":"TEST"});
        })
  })
  socket.on("get_info", function(data, fn){
      var list_of_table = new Array("UserStories", "Tasks", "Sprints");
      var list_of_queries = new Array("getUserStory", "getTask", "getSprint");
      var res_query = list_of_queries[list_of_table.indexOf(data.table)];
      var run_querys = conn.getQuery(res_query);
      run_querys.on("ready", function() {
        var reader = run_querys.execute({id:parseInt(data.id)});
        reader.on("ready", function(){
            res = reader.readAll();
            fn(res);
        })
      })
  })
  socket.on("getBacklogItems", function(data, fn){
      var run_querys = conn.getQuery("getTasksNotInSprint");
      run_querys.on("ready", function() {
        var reader = run_querys.execute();
        reader.on("ready", function(){
            res = reader.readAll();
            fn(res);
        })
      })
  })
  socket.on("getBacklogItemsSpecific", function(data, fn){
      var run_querys = conn.getQuery("getTasksInSprintOrNot");
      run_querys.on("ready", function() {
        var reader = run_querys.execute({sprint_id:data});
        reader.on("ready", function(){
            res = reader.readAll();
            fn({id:data, data:res});
        })
      })
  })
  socket.on("add_calendar_item", function(data, fn){
      var table = conn.getTable("Calendar");
      table.on("ready", function() {
          var res = {}
          for(val in data){
              res[val]=data[val];
          }
          var msg = "Created Meeting with " + name_sets["Calendar"] + " of " + res["title"];
      
          table.insertRow(res, function(data){
              fn(data, res);
              socket.broadcast.emit("mod_calendar", {id:data, data:res, newItem:true});
          })
          var table_rss = conn.getTable("RSS");
          table_rss.on("ready", function(){
            var data = {event:msg, timestamp:new Date().getTime()}
            var timestamp = new Date().getTime();
            var data = {event:msg, timestamp:timestamp}
            table_rss.insertRows(data, function(data){ 
                var itemID = parseInt(data);
            });
            socket.broadcast.emit("new_rss", {msg:msg, time:timestamp});
            socket.emit("new_rss", {msg:msg, time:timestamp});
          })
      })
  })  
  socket.on("update_calendar_item", function(data, fn){
      var table = conn.getTable("Calendar");
      table.on("ready", function() {
          var res = {}
          for(val in data){
              if(val !== "id"){
                res[val]=data[val];
              }
          }
          var msg = "Updated Meeting with " + name_sets["Calendar"] + " of " + res["title"];
          var id = data.id;
          table.updateRow(parseInt(data.id),res , function(data){
              fn(id, res);
              socket.broadcast.emit("mod_calendar", {id:id, data:res, newItem:false});
          })
          var table_rss = conn.getTable("RSS");
          table_rss.on("ready", function(){
            var data = {event:msg, timestamp:new Date().getTime()}
            var timestamp = new Date().getTime();
            var data = {event:msg, timestamp:timestamp}
            table_rss.insertRows(data, function(data){ 
                var itemID = parseInt(data);
            });
            socket.broadcast.emit("new_rss", {msg:msg, time:timestamp});
            socket.emit("new_rss", {msg:msg, time:timestamp});
          })
      })
  })
  socket.on("check_sprint", function(data, fn){
    var querys = conn.getQuery("getTasksInCurSprint");
    querys.on("ready", function() {
      var reader = querys.execute({sprint_id:data.id});
      var data_new = new Array();
      reader.on("ready", function() {
        data_new = reader.readAll();
        fn(data_new.length, data.id, data.name);
      });
    });  
  })
  socket.on("createNewUser", function(data){
    var table = conn.getTable("Users");
      table.on("ready", function() {
          var email = data.Email;
          var username = data.Username
          var newData = {Name:data.FirstName+" "+data.LastName, Role:data.Role, Email:data.Email};
          crypto.randomBytes(8, function(ex, buf) {
            var token=  buf.toString('base64');
            var crypto2 = require('crypto')
			, key = 'scrummgmttoolistoocoolforschool';
			var cipher2 = crypto2.createCipher('aes-256-cbc', key)
            cipher2.update(token, 'utf8', 'base64');
            var encryptedPassword = cipher2.final('base64') 
            data.Password = encryptedPassword;
            table.insertRow(data, function(){
                sendNewUserEmail(email, username, token);
                regen_users();
            });
            socket.broadcast.emit("newUser", newData);
            socket.emit("newUser", newData);
          });
      });
  })
  socket.on("testConnection", function(data, fn){
        fn();
    }) 
});

//New User
var nodemailer = require("nodemailer");
var sendNewUserEmail = function(email, uname, password){

    // create reusable transport method (opens pool of SMTP connections)
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: "Gmail",
        auth: {
            user: "scrummgmt@gmail.com",
            pass: "P@rticl3"
        }
    });

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "scrummgmt@gmail.com", // sender address
        to: email, // list of receivers
        subject: "Welcome to Scrum Management", // Subject line
        text: "Welcome to the Scrum Management Tool.  \nYour username is: "+uname + "\nYour password is: "+password, // plaintext body
        html: "<b>Welcome to the Scrum Management Tool</b><br/><br/><br/><div>Your username is: "+uname + "</div><div>Your password is: "+password + "</div>" // html body
    }

    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: " + response.message);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
}
var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT || "8080";
server.listen(port, ip);
module.exports = app;

/*
server.listen(3000);
module.exports = app;*/
var job = new cronJob('00 30 23 * * 1-5', function(){
    getBurnDownData();
    this.start();
  }, function () {
    // This function is executed when the job stops
  }, 
  true
);
var getBurnDownData = function(){
    var curr_sprint = 0, today = new Date();
    var querys = conn.getQuery("getAllSprints");
    var completed_sprint = false;
    querys.on("ready", function() {
      var reader = querys.execute({project_id:"1"});
      var data_new = new Array();
      reader.on("ready", function() {
        data_new = reader.readAll();
        for(value in data_new){
            var this_item = data_new[value];
            var start = new Date(this_item.StartDate);
            var end = new Date(this_item.EndDate);
            if(start <= today && today <= end){
                curr_sprint = this_item.id;
            }
            if(today === end){
                completed_sprint = true;
            }
        }
        var querys2 = conn.getQuery("getHoursForBurndown");
        querys2.on("ready", function() {
            var reader = querys2.execute({sprint_id:curr_sprint});
            var data_new2 = new Array();
            reader.on("ready", function() {
                data_new2 = reader.readAll();
                var totalHoursWorked = 0.0, totalHoursEstimated = 0.0;
                for(value in data_new2){
                    totalHoursWorked = totalHoursWorked + parseInt(data_new2[value].HoursWorked);
                    totalHoursEstimated = totalHoursEstimated + parseInt(data_new2[value].HoursEstimated);
                }
                today = new Date(today).getFullYear() + "-"+ (new Date(today).getMonth()+1) + "-" + new Date(today).getDate();
                var table = conn.getTable("BurnDowns");
                table.on("ready", function() {
                    table.insertRow({id_Sprints:curr_sprint, HoursWorked:totalHoursWorked, HoursEstimated:totalHoursEstimated, Date:today, isSprintTotal:"No"}, function(){
                        //
                    });
                    if(completed_sprint){
                        table.insertRow({id_Sprints:curr_sprint, HoursWorked:totalHoursWorked, HoursEstimated:totalHoursEstimated, Date:today, isSprintTotal:"Yes"}, function(){
                        //
                    });
                    }
                })
            });
        });
      });  
    }); 
}
job.start();