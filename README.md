Operating Instructions 


Creating a New User
Summary:
This article describes how to add a new user to the projects system.
Solution:
1)	Go to the Manage Tab
2)	Click New User
3)	Enter the User Information and Click Save 
4)	The user will check their email for their login credentials


Creating a New Sprint
Summary:
This article describes how to create a new Sprint.
Solution:
1)	Go to the Sprint Tab
2)	Click New Sprint
3)	Enter the Sprint Information 
4)	To edit the backlog see Creating the Sprint Backlog
5)	Click Save


Editing an existing Sprint
Summary:
This article describes how to edit an existing Sprint.
Solution:
1)	Go so the Sprint Tab
2)	Click the Pencil Icon next to the Sprint to be edited 
3)	Enter the Sprint Information
4)	To edit the backlog see Creating the Sprint Backlog
5)	Click Save


Creating the Sprint Backlog
Summary:
This article describes how to create the Sprint Backlog for any Sprint.
Solution:
1)	Go to the Sprints Tab
2)	Click the Pencil icon on the desired Sprint
3)	Go to the Build the Backlog Section 
4)	To add Tasks drag them from the Product Backlog to the Sprint Backlog 
5)	To remove Tasks drag them from the Sprint Backlog so Product Backlog
6)	Click Save


Claiming a Task in the Backlog
Summary:
This article describes how to claim a Task from the Sprint Backlog Page.
Solution:
1)	Go to the Sprint Log Tab
2)	Click and Drag the task from the Current Column to the desired Column 
3)	If the Task is dragged to Completed a dialog will display prompting the user to enter the Number of Hours Worked 
4)	If you click cancel the task will move back to the previous location


Creating a New User Story
Summary:
This article describes how to add a new User Story.
Solution:
1)	Go to the User Story Tab
2)	Click New User Story
3)	Enter the User Story Information and Click Save 
4)	To edit the Task see Editing a User Story or Task


Creating a New Task
Summary:
This article describes how to create a new Task.
Solution:
1)	Go to the User Stories Tab
2)	Click the Note icon next so the User Story to add a Task 
3)	Enter the Task Information and Click Save 
4)	To edit the Task see Editing a User Story or Task


Editing a User Story or Task
Summary:
This article describes how to edit existing user stories and tasks.
Solution: 
1)	Expand the target item
2)	Double click the item shat you want to change
3)	The edit field will open 
4)	Once you click off or tab off the value is saved


Creating/Edit a Calendar Event
Summary:
This article describes how to create a new calendar evens.
Solution:
1)	Go to the Hone Tab
2)	Drag over the time you want the time to be 
3)	Enter the information into the new Calendar Popup 
4)	To edit the event time drag ht to the new time
5)	To edit the event information double click the event and alter the information in the dialog


Using the Chat System
Summary:
This article describes how to use the chat system.
Solution:
1)	At the bottom of the screen click the chat notification box 
2)	In the chat dialog there is a list of users and the chat window itself 
 
